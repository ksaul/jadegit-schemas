<GUIClass name="GitRemoteListDialog" id="5270cad8-b0ee-4894-8362-43e9c338dc30">
    <superclass name="GitRepositoryDialog"/>
    <transient>true</transient>
    <subclassTransientAllowed>true</subclassTransientAllowed>
    <transientAllowed>true</transientAllowed>
    <ImplicitInverseRef name="btnAdd" id="0268f666-3602-464f-8ee6-50ea519e5944">
        <embedded>true</embedded>
        <type name="Button"/>
    </ImplicitInverseRef>
    <ImplicitInverseRef name="btnClose" id="2b8d8c1f-c303-4cb3-8035-225ba57c8bac">
        <embedded>true</embedded>
        <type name="Button"/>
    </ImplicitInverseRef>
    <ImplicitInverseRef name="btnEdit" id="c4bb2377-3f56-4a20-9042-8b13dfa1ec7d">
        <embedded>true</embedded>
        <type name="Button"/>
    </ImplicitInverseRef>
    <ImplicitInverseRef name="btnRemove" id="c55fffe7-cc01-4e21-b9da-de8466d9e451">
        <embedded>true</embedded>
        <type name="Button"/>
    </ImplicitInverseRef>
    <ImplicitInverseRef name="tblRemotes" id="daa0a40f-2dfc-4b7d-8826-a31e2dabc2da">
        <embedded>true</embedded>
        <type name="Table"/>
    </ImplicitInverseRef>
    <JadeMethod name="btnAdd_click" id="cc894713-85c6-4a86-b3cc-a2dbda7ae64c">
        <controlMethod name="Button::click"/>
        <methodInvocation>event</methodInvocation>
        <updating>true</updating>
        <source>btnAdd_click(btn: Button input) updating;

vars
	dialog : GitRemoteDialog;
	
begin
	dialog := GitRemoteDialog@open(repo);
	if dialog.showModal().Boolean then
		tblRemotes.refreshEntries(dialog.remote);
	endif;
	
epilog
	delete dialog;
end;</source>
        <Parameter name="btn">
            <usage>input</usage>
            <type name="Button"/>
        </Parameter>
    </JadeMethod>
    <JadeMethod name="btnClose_click" id="dc755320-1db9-40e9-bcff-bb640e5565ef">
        <controlMethod name="Button::click"/>
        <methodInvocation>event</methodInvocation>
        <updating>true</updating>
        <source>btnClose_click(btn: Button input) updating;

vars

begin
	unloadForm;
end;</source>
        <Parameter name="btn">
            <usage>input</usage>
            <type name="Button"/>
        </Parameter>
    </JadeMethod>
    <JadeMethod name="btnEdit_click" id="f9da14e0-7afa-4625-8719-2edf2974b8d1">
        <controlMethod name="Button::click"/>
        <methodInvocation>event</methodInvocation>
        <updating>true</updating>
        <source>btnEdit_click(btn: Button input) updating;

vars
	dialog : GitRemoteDialog;
	
begin
	dialog := GitRemoteDialog@open(tblRemotes.listObject());
	if dialog.showModal().Boolean then
		tblRemotes.refreshEntries(dialog.remote);
	endif;
	
epilog
	delete dialog;
end;</source>
        <Parameter name="btn">
            <usage>input</usage>
            <type name="Button"/>
        </Parameter>
    </JadeMethod>
    <JadeMethod name="btnEdit_enable" id="257b6d41-e6c5-40ae-a629-19fd22aad98c">
        <controlMethod name="Control::enable"/>
        <methodInvocation>event</methodInvocation>
        <updating>true</updating>
        <source>btnEdit_enable(cntrl: Control input):Boolean updating, protected;

begin
	cntrl.enabled := tblRemotes.listObject() &lt;&gt; null;
	return true;
end;
</source>
        <access>protected</access>
        <Parameter name="cntrl">
            <usage>input</usage>
            <type name="Control"/>
        </Parameter>
        <ReturnType>
            <type name="Boolean"/>
        </ReturnType>
    </JadeMethod>
    <JadeMethod name="btnOK_enable" id="af84de4a-02eb-4c63-be64-ae108c3967bb">
        <controlMethod name="Control::enable"/>
        <methodInvocation>event</methodInvocation>
        <updating>true</updating>
        <source>btnOK_enable(cntrl: Control input):Boolean updating, protected;

begin
	cntrl.visible := false;
	return true;
end;
</source>
        <access>protected</access>
        <Parameter name="cntrl">
            <usage>input</usage>
            <type name="Control"/>
        </Parameter>
        <ReturnType>
            <type name="Boolean"/>
        </ReturnType>
    </JadeMethod>
    <JadeMethod name="btnRemove_click" id="611b0ab4-1022-4516-b01f-1d2c77fb3c1a">
        <controlMethod name="Button::click"/>
        <methodInvocation>event</methodInvocation>
        <updating>true</updating>
        <source>btnRemove_click(btn: Button input) updating;

vars
	remote : GitRemote;
	
begin
	remote := tblRemotes.accessRow(tblRemotes.rows).itemObject.GitRemote;
	
	if app.msgBox("This will delete the '" &amp; remote.name &amp; "' remote and associated configuration." &amp; CrLf &amp; CrLf &amp;
				  "Are you sure you want to continue?", "Jade-Git", MsgBox_Yes_No + MsgBox_Stop_Icon + MsgBox_Default_Second) &lt;&gt; MsgBox_Return_Yes then
		return;
	endif;
	
	remote.remove();
end;
</source>
        <Parameter name="btn">
            <usage>input</usage>
            <type name="Button"/>
        </Parameter>
    </JadeMethod>
    <JadeMethod name="btnRemove_enable" id="f5304804-7042-49d8-82a4-eb2248f85a0e">
        <controlMethod name="Control::enable"/>
        <methodInvocation>event</methodInvocation>
        <updating>true</updating>
        <source>btnRemove_enable(cntrl: Control input):Boolean updating, protected;

begin
	cntrl.enabled := tblRemotes.listObject() &lt;&gt; null;
	return true;
end;
</source>
        <access>protected</access>
        <Parameter name="cntrl">
            <usage>input</usage>
            <type name="Control"/>
        </Parameter>
        <ReturnType>
            <type name="Boolean"/>
        </ReturnType>
    </JadeMethod>
    <JadeMethod name="load" id="3f068823-c90b-4468-97b2-0da332e2e409">
        <controlMethod name="Form::load"/>
        <methodInvocation>event</methodInvocation>
        <updating>true</updating>
        <source>load() updating;

begin
	inheritMethod();
	
	tblRemotes.setCellText( 1, 1, "Name" &amp; Tab &amp; "URL / Path" );
	tblRemotes.accessSheet(1).marginLeft := 2;
	tblRemotes.accessedSheet.marginRight := 2;
end;
</source>
    </JadeMethod>
    <JadeMethod name="tblRemotes_displayRow" id="ed13c817-7c2e-45c8-850a-fc87079eecde">
        <controlMethod name="Table::displayRow"/>
        <methodInvocation>event</methodInvocation>
        <updating>true</updating>
        <source>tblRemotes_displayRow(table: Table input; theSheet: Integer; remote: GitRemote; theRow: Integer; bcontinue: Boolean io):String updating;

begin
	return remote.name &amp; Tab &amp; remote.url();
end;</source>
        <Parameter name="table">
            <usage>input</usage>
            <type name="Table"/>
        </Parameter>
        <Parameter name="theSheet">
            <type name="Integer"/>
        </Parameter>
        <Parameter name="remote">
            <type name="GitRemote"/>
        </Parameter>
        <Parameter name="theRow">
            <type name="Integer"/>
        </Parameter>
        <Parameter name="bcontinue">
            <usage>io</usage>
            <type name="Boolean"/>
        </Parameter>
        <ReturnType>
            <type name="String"/>
        </ReturnType>
    </JadeMethod>
    <JadeMethod name="tblRemotes_populate" id="da32eb74-181c-4288-96fa-8978ca0af0c6">
        <controlMethod name="Control::populate"/>
        <methodInvocation>event</methodInvocation>
        <updating>true</updating>
        <source>tblRemotes_populate(cntrl: Control input; repo: GitRepository):Boolean updating, protected;

begin
	tblRemotes.displayCollection(repo.remotes, true, null, null);
	return true;
end;
</source>
        <access>protected</access>
        <Parameter name="cntrl">
            <usage>input</usage>
            <type name="Control"/>
        </Parameter>
        <Parameter name="repo">
            <type name="GitRepository"/>
        </Parameter>
        <ReturnType>
            <type name="Boolean"/>
        </ReturnType>
    </JadeMethod>
    <JadeMethod name="tblRemotes_rowColumnChg" id="6f15d53c-c6c4-429f-9361-0aae03ece2af">
        <controlMethod name="Table::rowColumnChg"/>
        <methodInvocation>event</methodInvocation>
        <updating>true</updating>
        <source>tblRemotes_rowColumnChg(table: Table input) updating;

vars

begin
	enableControls;
end;
</source>
        <Parameter name="table">
            <usage>input</usage>
            <type name="Table"/>
        </Parameter>
    </JadeMethod>
</GUIClass>
